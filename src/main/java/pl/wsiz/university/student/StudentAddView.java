package pl.wsiz.university.student;

import pl.wsiz.university.user.UserAddView;
import pl.wsiz.university.user.UserRepository;

import java.time.LocalDate;
import java.util.Scanner;

public class StudentAddView extends UserAddView<Student> {

    public StudentAddView(UserRepository userRepository) {
        super(userRepository);
    }

    @Override
    protected String getTitle() {
        return "DODAWANIE STUDENTA";
    }

    @Override
    protected Student getUserFromInput() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imię: ");
        String firstName = scanner.nextLine();

        System.out.println("Podaj nazwisko: ");
        String lastName = scanner.nextLine();

        System.out.println("Podaj email: ");
        String email = scanner.nextLine();

        System.out.println("Podaj hasło: ");
        String password = scanner.nextLine();

        System.out.println("Podaj dzień urodzenia: ");
        int day = scanner.nextInt();

        System.out.println("Podaj miesiąc urodzenia: ");
        int month = scanner.nextInt();

        System.out.println("Podaj rok urodzenia: ");
        int year = scanner.nextInt();

        System.out.println("Podaj nr albumu: ");
        long albumNumber = scanner.nextLong();

        Student student = new Student(firstName, lastName,
                email, password,
                LocalDate.of(year, month, day), albumNumber);

        return student;
    }

}
