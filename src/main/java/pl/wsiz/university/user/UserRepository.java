package pl.wsiz.university.user;

import pl.wsiz.university.user.User;

import java.util.List;

public interface UserRepository {

    void insert(User user);
    List<User> findAll();

}
