package pl.wsiz.university.user;

import java.util.List;
import java.util.Scanner;

public class LoginView {

    private UserRepository userRepository;

    public LoginView(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User login() {
        String email;
        String password;

        String autologin = System.getenv("USER_AUTOLOGIN");
        if (autologin != null && autologin.equals("true")) {
            email = System.getenv("USER_EMAIL");
            password = System.getenv("USER_PASSWORD");
        } else {
            System.out.println("============= EKRAN LOGOWANIA =============");
            System.out.println("Podaj adres email: ");
            Scanner scanner = new Scanner(System.in);
            email = scanner.nextLine();
            System.out.println("Podaj hasło: ");
            password = scanner.nextLine();
        }

        List<User> users = userRepository.findAll();
        User loggedUser = null;
        for (User user: users) {
            if (user.getEmail().equalsIgnoreCase(email)
                && user.matchesPassword(password)) {
                loggedUser = user;
            }
        }
        if (loggedUser == null) {
            System.out.println("Logowanie nieudane, spróbuj ponownie");
            return login();
        }
        return loggedUser;
    }

}
