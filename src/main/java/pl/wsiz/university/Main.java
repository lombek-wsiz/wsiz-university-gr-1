package pl.wsiz.university;

import pl.wsiz.university.administrator.Administrator;
import pl.wsiz.university.administrator.AdministratorMenuView;
import pl.wsiz.university.student.Student;
import pl.wsiz.university.teacher.Teacher;
import pl.wsiz.university.user.FileUserRepository;
import pl.wsiz.university.user.LoginView;
import pl.wsiz.university.user.User;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("Adam", "Kowalski", "adam@kowal.pl",
                "adam@er424",
                LocalDate.of(1992, 6, 20), 95614);
        Teacher teacher1 = new Teacher("Iwona", "Kowalski", "iwonan3@onet.pl",
                "iwona@$413",
                LocalDate.of(1982, 5, 30), "dr inż.");
        Administrator administrator1 = new Administrator("Jan", "Nowak",
                "janek@gmail.com", "nowakJan@4",
                LocalDate.of(1995, 8, 10));

        FileUserRepository fileUserRepository = new FileUserRepository();
        fileUserRepository.insert(student1);
        fileUserRepository.insert(teacher1);
        fileUserRepository.insert(administrator1);

        LoginView loginView = new LoginView(fileUserRepository);
        User loggedUser = loginView.login();
        System.out.println("Zalogowano jako: ");
        printUser(loggedUser);

        if (loggedUser instanceof Administrator) {
            new AdministratorMenuView(fileUserRepository).initialize();
        }
    }

    private static void printUser(User user) {
        System.out.println("Imię i nazwisko: " + user.getFirstName() + " " + user.getLastName());
        System.out.println("Adres email: " + user.getEmail());
        System.out.println("Data urodzenia: " + user.getDateOfBirth()
                .format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));

        // 1 przykład
        if (user instanceof Student) {
            System.out.println("Rola: Student");
        } else if (user instanceof Teacher) {
            System.out.println("Rola: Nauczyciel");
        } else if (user instanceof Administrator) {
            System.out.println("Rola: Administrator");
        }

        // 2 przykład - z metodą abstrakcyjną
        System.out.println("Rola: "+user.getRole());
    }

}