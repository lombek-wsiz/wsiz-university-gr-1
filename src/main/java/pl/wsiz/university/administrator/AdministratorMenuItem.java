package pl.wsiz.university.administrator;

public enum AdministratorMenuItem {

    USER_LIST(1, "lista użytkowników"),
    STUDENT_ADD(2, "dodaj studenta"),
    TEACHER_ADD(3, "dodaj nauczyciela"),
    ADMINISTRATOR_ADD(4, "dodaj administratora"),
    EXIT(5, "wyjście z programu");

    private int nr;
    private String description;

    AdministratorMenuItem(int nr, String description) {
        this.nr = nr;
        this.description = description;
    }

    public static AdministratorMenuItem convert(int chosen) {
        for (AdministratorMenuItem item: values()) {
            if (item.nr == chosen) {
                return item;
            }
        }
        return EXIT;
    }

    public int getNr() {
        return nr;
    }

    public String getDescription() {
        return description;
    }

}
