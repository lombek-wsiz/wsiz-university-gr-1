package pl.wsiz.university.administrator;

import pl.wsiz.university.user.User;

import java.time.LocalDate;

public class Administrator extends User {

    private static final long serialVersionUID = 1L;

    public Administrator(String firstName, String lastName,
                         String email, String password,
                         LocalDate dateOfBirth) {
        super(firstName, lastName, email, password, dateOfBirth);
    }

    @Override
    public String getRole() {
        return "Administrator";
    }

}
