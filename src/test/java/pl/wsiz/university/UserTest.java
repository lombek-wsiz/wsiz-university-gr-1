package pl.wsiz.university;

import org.junit.jupiter.api.Test;
import pl.wsiz.university.administrator.Administrator;
import pl.wsiz.university.user.User;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    void getFirstName() {
        User user1 = new Administrator("Adam", "Kowalski", "adam@kowal.pl",
                "adam@er424", LocalDate.of(1992, 6, 20));

        String firstName = user1.getFirstName();

        assertEquals("Adam", firstName);
    }

    @Test
    void getLastName() {
        User user1 = new Administrator("Adam", "Kowalski", "adam@kowal.pl",
                "adam@er424", LocalDate.of(1992, 6, 20));

        String lastName = user1.getLastName();

        assertEquals("Kowalski", lastName);
    }

}